#!/usr/bin/env python

import csv
import argparse

ap = argparse.ArgumentParser(description='Convert a CSV file into SQL INSERT statements')
ap.add_argument('csv', help='CSV filename')
ap.add_argument('table', help='SQL table name')
args = ap.parse_args()

with open(args.csv, 'rb') as csvfile:
	lines = csvfile.readlines()
	header = lines[0].strip()
	for i in range(1, len(lines)):
		s = """INSERT INTO %s (%s) VALUES (%s);""" % (args.table, header, lines[i].strip())
		print s
