CREATE TABLE dafif (
	id DECIMAL NOT NULL, 
	name VARCHAR(65) NOT NULL,
	city VARCHAR(33), 
	country VARCHAR(32), 
	iata VARCHAR(3), 
	icao VARCHAR(4), 
	wgs_dlat DECIMAL, 
	wgs_dlong DECIMAL, 
	altitude DECIMAL, 
	tz_offsetfromutc VARCHAR(5), 
	tz_dst VARCHAR(2), 
	timezone VARCHAR(30), 
	place_type VARCHAR(7), 
	data_source VARCHAR(11)
);
