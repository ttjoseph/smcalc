<?php
require("proj4.php");
require("db-credentials.php");

$db = new PDO($DB_CONNECTION_STRING, $DB_USER, $DB_PASSWORD);

$name_cache = array();
$warnings = array();

# Carriers for which there is no Medallion mileage bonus
$NO_ELITE_RDM_BONUS = array(
	"DL" => true,
	"VN" => true,
	"AR" => true,
	"IT" => true,
	"MH" => true,
	"DJ" => true,
	"CI" => true,
	"CZ" => true,
	"OK" => true,
	"GA" => true,
	"HA" => true,
	"WS" => true,
);

$ELITE_RDM_DOLLAR_MULTIPLIER = array(
	"peon" => 5,
	"silver" => 7,
	"gold" => 8,
	"platinum" => 9,
	"diamond" => 11
);

function add_warning($str)
{
	global $warnings;
	
	if(!in_array($str, $warnings))
		array_push($warnings, $str);
}

# Determines whether one longitude is east of another
function longitude_compare($a, $b)
{
	$a += 360.0;
	$b += 360.0;
	if($a >= 360)
		$a -= 360;
	if($b >= 360)
		$b -= 360;
		
	$diff = $b - $a;
	if((0 <= $diff and $diff < 180) # a is west of b
		or ($diff < -180))
		return -1;
	elseif((-180 <= $diff and $diff < 0) # a is east of b
		or ($diff > 180))
		return 1;
	else
		return 0;
}

# Given a list of stations, returns their longitudes
function get_longitudes($stations)
{
	global $db;
	
	if(strlen($stations[0]) == 3)
		$field = "iata";
	elseif(strlen($stations[0]) == 4)
		$field = "icao";
	else
	{
		print "Bug! get_longitudes was given bad input! (This isn't your fault.)";
		exit;
	}
	
	$query = "SELECT $field, wgs_dlong FROM dafif WHERE 0";
		
	foreach($stations as $station) {
        $station = $db->quote($station); // I know, I know...
		$query .= " OR $field = $station";
    }

	$lon = array();
	
	foreach($db->query($query) as $row)
	{
		$lon[$row[$field]] = $row['wgs_dlong'];
	}
	
	uasort($lon, "longitude_compare");
	
	#print "<pre>";print_r($lon);print"</pre>";#DEBUG
	
	return $lon;
}

# Returns HTML to print the abbreviated name of a station, along with a tooltip for its full name
function name_with_abbr($name)
{
	global $name_cache;

	if(isset($name_cache[$name]))
		return "<abbr title=\"" . $name_cache[$name] . "\">$name</abbr>";
	else
		return $name;
}

# Computes the great circle distance between two places.  We don't actually
# use this function anymore because it's not that accurate.
function great_circle_distance($lat1, $lon1, $lat2, $lon2)
{
	$lat1 = deg2rad($lat1);
	$lon1 = deg2rad($lon1);
	$lat2 = deg2rad($lat2);
	$lon2 = deg2rad($lon2);
	
	$earth_circ = 24859.82;
	$earth_radius = $earth_circ / (2 * 3.14159265);
	
	$dlon = $lon2 - $lon1;

	$a = cos($lat2) * sin($dlon);
	$a *= $a;
	
	$b = cos($lat1) * sin($lat2) - sin($lat1) * cos($lat2) * cos($dlon);
	$b *= $b;
	
	$c = sin($lat1) * sin($lat2) + cos($lat1) * cos($lat2) * cos($dlon);
	
	return atan2(sqrt($a + $b), $c) * $earth_radius;
}

# Determines whether two stations are the same (e.g. EHAM and AMS)
function airports_are_same($one, $two)
{
	global $db;
	
	if(strlen($one) != strlen($two))
	{
		// Check to see whether these are different identifiers for the same airport
		// (such as EHAM and AMS)
        $one_quoted = $db->quote($one); // Yes, I should be doing prepare and execute
        $two_quoted = $db->quote($two);
		$query = "SELECT iata, icao FROM dafif WHERE iata = $one_quoted OR iata = $two_quoted"
			. " OR icao = $one_quoted OR icao = $two_quoted";

		$res = $db->query($query);
		if($res)
			return true;
	} else if($one == $two)
		return true;

	return false;
}

# Returns the location of a station
function get_lat_lon($station)
{
	global $db;
	
    $station_quoted = $db->quote($station);
	if(strlen($station) == 3)
		$query = "SELECT icao, iata, name, city, country, wgs_dlat, wgs_dlong FROM dafif WHERE iata = $station_quoted LIMIT 1";
	elseif(strlen($station) == 4)
		$query = "SELECT icao, iata, name, city, country, wgs_dlat, wgs_dlong FROM dafif WHERE icao = $station_quoted LIMIT 1";
	else
	{
		# print "I don't know what $station is.";
		return null;
	}
	
	$res = $db->query($query);
	$row = $res->fetch();
	if(row == false)
	{
		# print "I don't know where $station is.";
		return null;
	}
	
	global $name_cache;

	$full_name = implode(", ", array($row["name"], $row["city"], $row["country"]));
	if(isset($row["iata"])) $name_cache[$row["iata"]] = $full_name;
	if(isset($row["icao"])) $name_cache[$row["icao"]] = $full_name;

	$lat = $row["wgs_dlat"] + 0;
	$lon = $row["wgs_dlong"] + 0;

	return array($lat, $lon);
}

# Avoids using geod at all to get a bunch of distances.
function fill_in_multiple_airport_distances($segment_list)
{
	$distances = array();
	foreach ($segment_list as $segment)
	{
		$stations = explode("-", $segment);
		$orig = get_lat_lon($stations[0]);
		$dest = get_lat_lon($stations[1]);
		if($orig == null || $dest == null)
		{
			$dist = -1;
		} else 
		{
			$dist = distance_in_miles($orig[0], $orig[1], $dest[0], $dest[1]);
		}
		$distances[$segment] = $dist;
	}
	return $distances;
}

# Given a list of segments, forks one instance of geod to find all the distances.
# This is, visibly, far more efficient than forking one geod for every distance.
function fill_in_multiple_airport_distances_old($segment_list)
{
	$descriptorspec = array(
		0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
		1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
		2 => array("file", "/tmp/error-output.txt", "a") // stderr is a file to write to
	);

	$process = proc_open('/usr/bin/geod +ellps=WGS84 -I +units=us-mi', $descriptorspec, $pipes);

	if (is_resource($process))
	{

		foreach ($segment_list as $segment)
		{
			$stations = explode("-", $segment);
			#DEBUG
			#print("[" . $stations[0] . " " . $stations[1] . "]");
			$orig = get_lat_lon($stations[0]);
			$dest = get_lat_lon($stations[1]);			
			fwrite($pipes[0], $orig[0] . " " . $orig[1] . " " . $dest[0] . " " . $dest[1] . "\n");
		}
	} else
	{
		print "Can't run distance finder.  This is bad, but not your fault.";
		exit;
	}
	
	fflush($pipes[0]);
  fclose($pipes[0]);

	$distances = array();
#  echo "<pre>";
	$count = 0;
	while($line = fgets($pipes[1]))
	{
#		echo $line; #DEBUG
		$dist = preg_split("/\s+/", $line);
		$distances[$segment_list[$count]] = $dist[2];
		$count++;
	}
#	print "</pre>";

  fclose($pipes[1]);
  // It is important that you close any pipes before calling
  // proc_close in order to avoid a deadlock
  $return_value = proc_close($process);
	return $distances;
}

# Get a single distance
function proj_distance($lat1, $lon1, $lat2, $lon2)
{
	return distance_in_miles($lat1, $lon1, $lat2, $lon2);
}

# Get a single distance using geod. Inefficient to call this a lot!
function proj_distance_old($lat1, $lon1, $lat2, $lon2)
{
	$out = exec("echo '$lat1 $lon1 $lat2 $lon2' | /usr/bin/geod +ellps=clrk66 -I +units=us-mi");
	$dist = preg_split("/\s+/", $out);
	return $dist[2];
}

function get_db_handle()
{
	static $db = 0;
	if($db == 0)
		$db = sqlite_open("../smcalc/airports.db", 0666, $err_msg);

	if($db == FALSE)
	{
		print "$err_msg\n";
		exit;
	}
	
	return $db;	
}

function airport_distance($from, $to)
{
	global $db;

	$orig = get_lat_lon($from);
	$dest = get_lat_lon($to);
	if($orig == null || $dest == null) return null;
	
	return distance_in_miles($orig[0], $orig[1], $dest[0], $dest[1]);
}

# Returns 500 if $miles is less than 500
# Reflects a 500-mile minimum for FF miles awarded
function segment_min($miles, $carrier, $fare = "?")
{
	# Check if, depending on the airline, it would be better to have th fare multiplier
	# instead of a 500 mile minimum
	
	switch($carrier)
	{
	# case "CZ":
	# case "SQ":
	case "AV":
		return $miles;
	
	case "LY":
		print "I don't know how to handle LY flights!";
		exit;
		
	default:
		return ($miles > 0 and $miles < 500) ? 500 : $miles;
	}
}

# Updates elite status according to current EQMs
# Eventually, this will consult some kind of table for qualification thresholds
function update_elite_status($eqm, $carrier)
{
	global $elite, $ELITE_RDM_DOLLAR_MULTIPLIER;
	$elite_num = 0;
	if(isset($ELITE_RDM_DOLLAR_MULTIPLIER[$elite])) {
		$elite_num = $ELITE_RDM_DOLLAR_MULTIPLIER[$elite];
	}
	
	if($eqm >= 125000 && $elite_num < $ELITE_RDM_DOLLAR_MULTIPLIER["diamond"])
		$elite = "diamond"; 
	else if($eqm >= 75000 && $elite_num < $ELITE_RDM_DOLLAR_MULTIPLIER["platinum"])
		$elite = "platinum";
	else if($eqm >= 50000 && $elite_num < $ELITE_RDM_DOLLAR_MULTIPLIER["gold"])
		$elite = "gold";
	else if($eqm >= 25000 && $elite_num < $ELITE_RDM_DOLLAR_MULTIPLIER["silver"])
		$elite = "silver";
}

# Returns elite multiplier miles
function elite_bonus_rdm($miles, $carrier, $fare)
{
	global $elite, $NO_ELITE_RDM_BONUS;
	
	if(isset($NO_ELITE_RDM_BONUS[$carrier])) return 0;

	switch($carrier)
	{
	case "AM":
	case "AF": # Air France
	case "MU": # China Eastern
	case "AZ":
	case "9W":
	case "G3":
	case "KL":
	case "VS":
	case "VA":
	case "UX":
	case "KQ":
	case "ME":
	case "SV":
	case "RO": # TAROM
		switch($elite)
		{
		case "silver": $bonus = 0.4; break;
		case "gold": $bonus = 0.6; break;
		case "platinum": $bonus = 0.8; break;
		case "diamond": $bonus = 1.2; break;
		}
		break;
	
	default:
		switch($elite)
		{
		case "silver":
			$bonus = 0.25;
			break;
		case "gold":
		case "platinum":
			$bonus = 1.0;
			break;
		case "diamond":
			$bonus = 1.25;
			break;
		default:
			$bonus = 0.0;
		}
	}
	
	return round($miles * $bonus);
}

# Returns the number of elite-qualifying miles for a physical distance, carrier, and fare bucket
function eqm($miles, $carrier, $fare, $metal = "??")
{
	$miles = segment_min($miles, $carrier);
	$multiplier = 0;

	switch($carrier)
	{
	case "DL":
		if(!in_array($metal, array("DL", "AS", "VA", "AF"))) {
			print "I don't know how to handle $carrier flights on $metal metal. You might try entering it simply as a flight on $metal, rather than as a codeshare.";
			return;
		}
		$multiplier = get_multiplier_by_fare($fare, "FJ", 2.0, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "APGCDIZYB", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WSMHQKLUTXVE", 1.0, $multiplier);

		# Other airlines get more miles for W fares
		if(in_array($metal, array("AS", "VA"))) {
			$multiplier = get_multiplier_by_fare($fare, "W", 1.5, $multiplier);
		}
		break;
	case "RO": # TAROM
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YB", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MUKR", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HGL", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "QNTVSE", 0.25, $multiplier);
		break;
	case "ME": # Middle East Airlines
		$multiplier = get_multiplier_by_fare($fare, "JCDIZ", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UKH", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "LQT", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "NRV", 0.25, $multiplier);
		break;
	case "AR": # Aerolineas Argentinas
		$multiplier = get_multiplier_by_fare($fare, "JC", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "DIWS", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Y", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "BM", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UKHLQ", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "TEAGVNR", 0.25, $multiplier);
		break;
	case "SV": # Saudia
		$multiplier = get_multiplier_by_fare($fare, "FPA", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YEB", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MKH", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "LQT", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "GNVU", 0.25, $multiplier);
		break;
	case "GA": # Garuda Indonesia
		$multiplier = get_multiplier_by_fare($fare, "FAP", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YB", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "M", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "KN", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "QT", 0.25, $multiplier);
		break;
	case "MF": # Xiamen Airlines
		$multiplier = get_multiplier_by_fare($fare, "FA", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YHBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "L", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "K", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "NQV", 0.25, $multiplier);
		break;
	case "WS": # WestJet
		$multiplier = get_multiplier_by_fare($fare, "JCD", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WOR", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YB", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "M", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HQNSX ", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "TKL", 0.25, $multiplier);
		break;

	case "SU": # Aeroflot
		$multiplier = get_multiplier_by_fare($fare, "JCDIZ", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WSA", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YB", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MUKH", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "TL", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "QEN", 0.25, $multiplier);
		break;
	case "AM": # Aeromexico
		$multiplier = get_multiplier_by_fare($fare, "J", 2.0, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDIWY", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "BMKHQLTUENRV", 1.0, $multiplier);
		break;
	case "AF": # Air France
		$multiplier = get_multiplier_by_fare($fare, "PF", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "J", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDIZ", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "O", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WSA", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UK", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HLQTEN", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "RGVX", 1, $multiplier);
		break;
	case "AS":
		$multiplier = get_multiplier_by_fare($fare, "F", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YSBMHQLVKGTU", 1.0, $multiplier);
		break;
	case "AZ": # Alitalia
		$multiplier = get_multiplier_by_fare($fare, "J", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDIE", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "AP", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HKVT", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "NSQXG", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "RFOLW", 1, $multiplier);
		break;
	case "CI": # China Airlines
		$multiplier = get_multiplier_by_fare($fare, "F", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "JCD", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WUAE", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "K", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "VT", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "RQHN", 0.25, $multiplier);
		break;
	case "OK": # Czech Airlines
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HKT", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "ALQ", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UX", 0.25, $multiplier);
		break;
	case "KL":
		$multiplier = get_multiplier_by_fare($fare, "J", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDIZ", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "O ", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UKWPF", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HLQTENSA", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "RGVX", 1, $multiplier);
		break;
	case "KE": # Korean again gives MQMs
		$multiplier = get_multiplier_by_fare($fare, "PF", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "J", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDIR", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WYB", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "S", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MHE", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "GQ", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "KLU ", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "T ", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "N ", 1, $multiplier);
		break;
	case "RO": # TAROM
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YB", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MUKR", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HGL", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "QNTVSE", 0.25, $multiplier);
		break;
	case "UX": # Air Europa
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "L", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "KHVE", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "QRSUTF", 0.25, $multiplier);
		break;
	case "KQ": # Kenya Airways
		$multiplier = get_multiplier_by_fare($fare, "JCDIZ", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UKH", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "LNQ", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "RETW", 0.25, $multiplier);
		break;
	case "MH": # Malaysia Airlines
		$multiplier = get_multiplier_by_fare($fare, "FPACJDE", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YKMBQHSUOT", 1.0, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "GLVW", 0.0, $multiplier);
		break;
	case "YX": # Midwest Airlines
		$multiplier = get_multiplier_by_fare($fare, "YBHVQKWMTROP", 1.0, $multiplier);
		break;
	case "CZ": # China Southern
		$multiplier = get_multiplier_by_fare($fare, "F", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WS", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Y", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "PBMHK", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UALEQ", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "VZTNR", 0.25, $multiplier);
		break;
	case "VA": # Virgin Australia
		$multiplier = get_multiplier_by_fare($fare, "F", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "A", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "J", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDI", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WRO", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YB", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HK", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "LENVQTUSM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "G", 1, $multiplier);
		break;

	case "VN": # Vietnam Airlines
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WZU†", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Y", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MSB", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HKL", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Q", 0.25, $multiplier);
		break;

	case "MU": # China Eastern
		if($fare == "P") {
			add_warning("P class on China Eastern is assumed to be international. If it's within China, miles earned are fewer.");
		}
		$multiplier = get_multiplier_by_fare($fare, "U", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "F", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "P", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "J", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDQI", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "W", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Y", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "B", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MEH", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "KLNRSV", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "TGZ", 1, $multiplier);
		break;
		
	case "VS": # Virgin Atlantic
		$multiplier = get_multiplier_by_fare($fare, "J", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDIZ", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WSKH", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "V", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YB", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "RLU", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MEQX", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "NO", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "T", 1, $multiplier);
		break;

	case "G3": # GOL
		$multiplier = get_multiplier_by_fare($fare, "CD", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Y", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "TJWPKMHQEL", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "VSB", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "FAUN", 1, $multiplier);
		break;

	case "9W": # Jet Airways
		$multiplier = get_multiplier_by_fare($fare, "FA", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CJZ", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "IP", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YM", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "TU", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "QSK", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HV", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "OW", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "NL", 1, $multiplier);
		break;

	case "HA": # Hawaiian
		$multiplier = get_multiplier_by_fare($fare, "FPJC", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Y", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WVX", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "QSN", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HIBMGLK", 0.25, $multiplier);
		break;

	# Some carriers do not award MQMs
	case "OA": # Olympic Air
	case "AV":
	case "IT": # Kingfisher Airlines
	
	default:
		print "I don't know the carrier $carrier.";
		exit;
	}

	return round($miles * $multiplier);
}

# Get multiplier according to fare bucket
function get_multiplier_by_fare($fare, $list, $found, $notfound)
{
	if($fare == "") return $notfound;
	if(strpos($list, $fare) === false)
		return $notfound;
	else
		return $found;
}

# Redeemable miles (incl. multiplier)
function rdm($miles, $carrier, $fare)
{
	$miles = segment_min($miles, $carrier);
	$multiplier = 0;
        $elite_bonus_multiplier = 1.0;
	$fare = trim($fare);
	
	switch($carrier)
	{
	case "DL":
		# DL tickets only earn RDMs by revenue now. Boooo
		break;
	case "KL":
		$multiplier = get_multiplier_by_fare($fare, "J", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDIZ", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "O ", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UKWPF", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HLQTENSA", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "RGVX", 0.25, $multiplier);
		break;
	case "ME": # Middle East Airlines
		$multiplier = get_multiplier_by_fare($fare, "JCDIZ", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UKH", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "LQT", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "NRV", 0.25, $multiplier);
		break;
	case "AR": # Aerolineas Argentinas
		$multiplier = get_multiplier_by_fare($fare, "JC", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "DIWS", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Y", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "BM", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UKHLQ", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "TEAGVNR", 0.25, $multiplier);
		break;
	case "SV": # Saudia
		$multiplier = get_multiplier_by_fare($fare, "FPA", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YEB", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MKH", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "LQT", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "GNVU", 0.25, $multiplier);
		break;
	case "GA": # Garuda Indonesia
		$multiplier = get_multiplier_by_fare($fare, "FAP", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YB", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "M", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "KN", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "QT", 0.25, $multiplier);
		break;
	case "MF": # Xiamen Airlines
		$multiplier = get_multiplier_by_fare($fare, "FA", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YHBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "L", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "K", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "NQV", 0.25, $multiplier);
		break;
	case "WS": # WestJet
		$multiplier = get_multiplier_by_fare($fare, "JCD", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WOR", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YB", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "M", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HQNSX ", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "TKL", 0.25, $multiplier);
		break;
	case "SU": # Aeroflot
		$multiplier = get_multiplier_by_fare($fare, "JCDIZ", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WSA", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YB", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MUKH", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "TL", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "QEN", 0.25, $multiplier);
		break;
	case "AM": # Aeromexico
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 2.0, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WY", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "BMKHQLTU", 1.0, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "ENR", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "V", 0.25, $multiplier);
		break;
	case "AF":  # Air France
		$multiplier = get_multiplier_by_fare($fare, "PF", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "J", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDIZ", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "O", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WSA", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UK", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HLQTEN", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "RGVX", 0.25, $multiplier);
		break;
	case "AS":
		$multiplier = get_multiplier_by_fare($fare, "F", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YSBMHQLVKGTU", 1.0, $multiplier);
		break;
	case "AZ":
		$multiplier = get_multiplier_by_fare($fare, "J", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDIE", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "AP", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HKVT", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "NSQXG", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "RFOLW", 0.25, $multiplier);
		break;
	case "OK":
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HKT", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "ALQ", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UX", 0.25, $multiplier);
		break;
	case "KE": # Korean
		$multiplier = get_multiplier_by_fare($fare, "PF", 3, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "J", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDIR", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WYB", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "S", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MHE", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "GQ", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "KLU ", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "T ", 0.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "N ", 0.25, $multiplier);
		break;
	case "RO": # TAROM
		$multiplier = get_multiplier_by_fare($fare, "CDJS", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBMEGHNTWKRLQUV", 1.0, $multiplier);
		break;
	case "NW":
		add_warning("Does Northwest exist any more?");
		$multiplier = get_multiplier_by_fare($fare, "PJCZF", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBIMHQVSKTL", 1.0, $multiplier);
		break;
	case "UX": # Air Europa
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "L", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "KHVE", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "QRSUTF", 0.25, $multiplier);
		break;
	case "HA": # Hawaiian
		# add_warning("Mileage credit for Hawaiian Airlines depends on the route flown, which this calculator is not smart enough to figure out. The results for this trip may be wrong.");
		$multiplier = get_multiplier_by_fare($fare, "FPJC", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Y", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WVX", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "QSN", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HIBMGLK", 0.25, $multiplier);
		break;
	case "CI": # China Airlines
		$multiplier = get_multiplier_by_fare($fare, "F", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "JCD", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WUAE", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "K", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "VT", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "RQHN", 0.25, $multiplier);
		break;

	case "MU": # China Eastern
		$multiplier = get_multiplier_by_fare($fare, "U", 3, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "F", 3, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "P", 3, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "J", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDQI", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "W", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Y", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "B", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MEH", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "KLNRSV", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "TGZ", 0.25, $multiplier);
		break;

	case "CZ": # China Southern
		$multiplier = get_multiplier_by_fare($fare, "F", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WS", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Y", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "PBMHK", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UALEQ", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "VZTNR", 0.25, $multiplier);
		break;

	case "G3": # GOL
		$multiplier = get_multiplier_by_fare($fare, "CD", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Y", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "TJWPKMHQEL", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "VSB", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "FAUN", 0.25, $multiplier);
		break;

	case "IT": # Kingfisher Airlines
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YKBMONLQWVSHGU", 1.0, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "EXZRT", 0.0, $multiplier);
		add_warning("Delta's partnership with Kingfisher is terminating as of Oct 1, 2012. Mileage earned depends on the particular routes flown, so expect that this estimate is wrong.");
		break;

	case "9W": # Jet Airways
		$multiplier = get_multiplier_by_fare($fare, "FA", 3, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CJZ", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "IP", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YM", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "TU", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "QSK", 0.75, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HV", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "OW", 0.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "NL", 1, $multiplier);
		break;

	case "KQ": # Kenya Airways
		$multiplier = get_multiplier_by_fare($fare, "JCDIZ", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UKH", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "LNQ", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "RETW", 0.25, $multiplier);
		break;

	case "MH": # Malaysia Airlines
		$multiplier = get_multiplier_by_fare($fare, "AFP", 2.0, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CJD", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YWU", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "BK", 1.0, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MQH", 0.7, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "SV", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "OL", 0.3, $multiplier);
		add_warning("For some fare classes, mileage is only accurate for international segments.");
		break;

	case "YX": # Midwest Airlines
		$multiplier = get_multiplier_by_fare($fare, "YBHVQKWMTROP", 1.0, $multiplier);
		break;

	case "VN": # Vietnam Airlines
		$multiplier = get_multiplier_by_fare($fare, "JCDI", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WZU†", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Y", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MSB", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HKL", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "Q", 0.25, $multiplier);
		break;

	case "VA": # Virgin Australia International
		$multiplier = get_multiplier_by_fare($fare, "F", 3, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "A", 3, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "J", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDI", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WRO", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YB", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HK", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "LENVQTUSM", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "G", 0.25, $multiplier);
		break;

	case "VS": # Virgin Atlantic
		$multiplier = get_multiplier_by_fare($fare, "J", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDIZ", 2, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "WSKH", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "V", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YB", 1.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "RLU", 1, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "MEQX", 0.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "NO", 0.25, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "T", 0.25, $multiplier);
		break;
	
	case "OA": # Olympic Air
		$multiplier = get_multiplier_by_fare($fare, "CDZWKQLMY", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "XBVNSEOUTA", 1.0, $multiplier);
		break;

	case "RO": # TAROM
		$multiplier = get_multiplier_by_fare($fare, "J", 1.75, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "CDI", 1.5, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "YBM", 1.0, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "UKR", 0.75, $multiplier);
		$multiplier = get_multiplier_by_fare($fare, "HGLQNTVSE", 0.5, $multiplier);
		break;
		
	}
	
	# FIXME
	#else if($multiplier == 1.0) # 500-mile minimum only seems to apply in economy
	#	$miles = segment_min($miles, $carrier);
		
	#	print "(" .round($miles * $multiplier) . ")" . "[" .elite_bonus_rdm($miles, $carrier, $fare) ."]";
	return round($miles * $multiplier) 
		+ $elite_bonus_multiplier * elite_bonus_rdm(segment_min($miles, $carrier), $carrier, $fare);
}

function c($class, $string)
{
	return "<span class=\"$class\">$string</span>";
}

function emit_google_analytics_snippet()
{
?>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-574670-4");
pageTracker._trackPageview();
} catch(err) {}</script>
<?php
}
