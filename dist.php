<?php
$ajax = isset($_GET['ajax']);
if(!$ajax) { ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>Unofficial SkyMiles calculator - results</title>
	<link rel="stylesheet" href="site.css" type="text/css" media="screen" title="" charset="utf-8">
</head>
<body>
<?php
}
require("site.php");

if($ajax) header("Content-type: text/plain");

function prop($getvar, $default)
{
	if(isset($_GET[$getvar]))
		return $_GET[$getvar];
	else
		return $default;
}

if(!$ajax) print "<h1>Unofficial SkyMiles calculator &mdash; results</h1>\n";

$total_gc_miles = 0;

$total_mqm = str_replace(",", "", prop("start_mqm", 0));
update_elite_status($total_mqm, "DL");
$total_rdm = str_replace(",", "", prop("start_rdm", 0));
$elite = prop("elite", "peon");
$default_carrier = strtoupper(prop("default_carrier", "DL"));
$default_fare = strtoupper(prop("default_fare", "T"));

$whole_route = trim(prop("route", ""));
$lines = preg_split("/[\n\r]+/", $whole_route);

if(!$ajax) {
	print "<div class=\"results\">\n";
	print "Beginning with " . c("miles-eqm", $total_mqm) . " MQM, "
		. c("miles-rdm", $total_rdm) . " redeemable miles, and " 
		. c("elite-". $elite, $elite) . " status.";
	print "<hr/>";
}

foreach ($lines as $line)
{
	# Allow user to specify negative numbers for bonus miles
	# JFK-5000: gained 5000 miles
	# JFK--5000: lost 5000 miles
	# JFK- -5000: lost
	# JFK -5000: lost
	$line = preg_replace("/[\\-\s]\\-([0-9])/", '-_$1', $line);
	$line = preg_replace("/^-/", '_', $line);
	
	# Grab a line of the whole route and tokenize it.
	$tokens = preg_split("/[\\-\s]+/", trim($line));

	$orig = "";
	$dest = "";
	$gc_miles = $mqm = $rdm = 0;

	$carrier = $metal = $default_carrier;
	# We keep track of whether any non-Delta carrier was involved in this trip, for revenue-based RDM calculations
	$not_delta = ($carrier != "DL");
	$fare = $default_fare;
	$dollars = array();

	update_elite_status($total_mqm, $carrier);

	############ Main token processing loop ############
	foreach ($tokens as $tok)
	{
		# If it contains a period, it's a fare bucket specifier
		# If it's 3 alpha letters, it's an IATA station identifier
		# If it's 4 alpha letters, it's an ICAO station identifier
	
		$tok = strtoupper($tok);
		$tok_len = strlen($tok);


		# If it's a dollar amount, save it
		if (preg_match("/^\\$([0-9]+\.?[0-9]*)$/", $tok, $matches))
		{
			# This is the weird PHP syntax for pushing to an array without a function call
			$dollars[] = $matches[1] + 0;
		}
		# If it's a number, treat it as bonus miles, which are not MQMs
		else if(preg_match("/^[\\+\\_]?[0-9]+$/", $tok))
		{
			$tok = preg_replace("/_/", "-", $tok);
			$tok = $tok + 0;
			$rdm += $tok;
			if($tok != 0)
			{
				if($tok > 0) {
					if(!$ajax) print "<b>Bonus</b>: Received ";
				}
				else
				{
					$tok = -$tok;
					if(!$ajax) print "<b>Deduction</b>: Spent ";
				}
				if(!$ajax) print c("miles-rdm", $tok) . " redeemable miles.<br/>\n";
			}
		}		
		else if($tok_len == 3 or $tok_len == 4)
		{
			# Check for period denoting fare bucket
			if(preg_match("/([A-Z0-9][A-Z0-9])\.([A-Z])/", $tok, $matches))
			{
				$carrier = $metal = $matches[1];
				$fare = $matches[2];
			} else if($orig != "") # We already had an origin, so grab the dest and calcuate the distance
			{
				$dest = $tok;
				# Move on to the next token if the origin and destination are the same
				if(airports_are_same($orig, $dest))
				{
					# Allows routes of the form "JFK-MIA MIA-JFK" without "MIA-MIA" being considered
					# a valid segment
					continue;
				}


				$miles = round(airport_distance($orig, $dest));
				if($miles < 0 || $miles == null)
				{
					if($ajax) {
						print "{}";
					} else {
						if(get_lat_lon($orig) == null) {
							print "$orig&ndash;$dest: Sorry, I don't know the airport $orig.<br/>\n";
						}
						if(get_lat_lon($dest) == null) {
							print "$orig&ndash;$dest: Sorry, I don't know the airport $dest.<br/>\n";
						}

					}
					exit;
				}
				$gc_miles += $miles;
				$mqm += eqm($miles, $carrier, $fare, $metal);
				$rdm += rdm($miles, $carrier, $fare);
				# If the carrier at this time is not Delta, that means we can't calculate revenue-based RDMs
				if($carrier != "DL") {
					$not_delta = true;
				}
			
				if(!$ajax) {
					print c("station", name_with_abbr($orig)) . "&ndash;" 
						. c("station", name_with_abbr($dest)) . ": " 
						. c("miles-gc", $miles) . " miles on $carrier";
					if($carrier != $metal)
						print " on $metal metal";
					print " in $fare.  As a " . c("elite-" . $elite, $elite);
					print ", earned " . c("miles-eqm", eqm($miles, $carrier, $fare, $metal)) . " MQM and " 
						. c("miles-rdm", rdm($miles, $carrier, $fare)) . " redeemable miles.<br/>\n";
				}
				$orig = $dest;
			
				update_elite_status($mqm + $total_mqm, $carrier);

				# Reset fare bucket and carrier, in preparation for the next orig/dest pair
				$carrier = $metal = $default_carrier;
				$fare = $default_fare;
			
			}
			else
				$orig = strtoupper($tok);
		}
		else if($tok_len == 6)
		{
			# Check for AABB.Y - meaning, coded as AA but on BB metal with Y fare
			if(preg_match("/([A-Z0-9][A-Z0-9])([A-Z0-9][A-Z0-9])\.([A-Z])/", $tok, $matches))
			{
				$carrier = $matches[1];
				$metal = $matches[2];
				$fare = $matches[3];				
			}
		} else if($tok_len == 2)
		{
			if(!$ajax) {
				print "Sorry, I don't understand <code>$line</code>.<br/><br/>";
				print "It looks like you were trying to say you flew on $tok.  But you also need to specify a fare class: <code>$tok.Y</code> for example.";
			}
			exit;
		}
		else if($tok_len == 1) # Definitely a fare bucket
		{
			$carrier = $default_carrier;
			$fare = $tok;
		} else
		{
			if(!$ajax) {
				print "Sorry, I don't understand <code>$line</code>.<br/><br/>";
				print "I don't know what <code>$tok</code> means!";
			}
			exit;
		}

	}

	# Now we are done with a line which represents a trip
	$rdm_cost_string = $mqm_cost_string = "";
	# If the user specified multiple dollar values, assume only the first is to be used for RDM calculation
	if(count($dollars) > 0)
	{
		$non_tax_dollars = $dollars[0];
		$total_dollars = array_sum($dollars);
		if(!$ajax) {
			print "Paid \$$total_dollars for this trip.<br/>\n";
		}
		global $ELITE_RDM_DOLLAR_MULTIPLIER;
		if(!$not_delta) {
			$revenue_rdm = $ELITE_RDM_DOLLAR_MULTIPLIER[$elite] * $non_tax_dollars;
			if($revenue_rdm > 75000) {
				$revenue_rdm = 75000;
			}
			
			if(!$ajax) {
				print "As a " . c("elite-" . $elite, $elite) . ", "
				    . "earned " . c("miles-rdm", $revenue_rdm) . " redeemable miles for \$$non_tax_dollars,"
				    . " assuming that is the <i>non-tax</i> part of the fare.<br/>";
				if(count($dollars) == 1) {
					print "Hint: If you enter another dollar amount on the same line, only the first will be used for the redeemable miles calculation, and the sum total will be used to calculate cost per mile.<br/>";
				}	
				if($revenue_rdm >= 75000) {
					print "There is a 75,000 mile per ticket limit on redeemable miles earned by dollars paid.<br/>";
				}
			}
			$rdm += $revenue_rdm;
		}
		$rdm_cost_string = sprintf(" at \$%.3f each", $total_dollars / $rdm);
		$mqm_cost_string = sprintf(" at \$%.3f each", $total_dollars / $mqm);
	} elseif(!$not_delta and !$ajax) {
		print "Hint: Enter the fare paid to estimate redeemable miles. See the previous page for details.<br/>";
	}
	
	if(count($warnings) > 0)
	{
		if(!$ajax) {
			print "<div style=\"background: yellow; padding-right: 1em; display: inline-block;\"><ul>";
			foreach ($warnings as $warning) {
				print "<li>$warning</li>\n";
			}
			print "</ul></div>\n";
		}
	}
	$warnings = array();

	$total_gc_miles += $gc_miles;
	$total_mqm += $mqm;
	$total_rdm += $rdm;
	
	if(!$ajax) {
		print "<table><tr><td class=\"interim-summary\">";
		print "Miles flown: " . c("miles-gc", $gc_miles)
			. " this trip, " . c("miles-gc", $total_gc_miles) . " so far<br/>\n";
		print "MQM earned: " . c("miles-eqm", $mqm) ." this trip$mqm_cost_string, " 
			. c("miles-eqm", $total_mqm) . " total so far<br/>\n";
		print "Redeemable miles earned: " . c("miles-rdm", $rdm)
		. " this trip$rdm_cost_string, " . c("miles-rdm", $total_rdm) . " total so far<br/>\n";
		print "</td></tr></table><br/>\n";
	}
}

if(!$ajax) {
	print "<hr/><table><tr><td class=\"final-summary\">";
	print "Total miles actually flown: " . c("miles-gc", $total_gc_miles) . "<br/>\n";
	print "Total MQM: " . c("miles-eqm", $total_mqm) . "<br/>\n";
	print "Total redeemable miles: " . c("miles-rdm", $total_rdm) . "<br/>\n";
	print "Status now: " . c("elite-" . $elite, $elite) . "<br/>";
	print "</td></tr></table>";
}

if($ajax && $total_gc_miles <= 0)
	exit;

if($ajax) {
	print "{\"miles_flown\": $total_gc_miles, \"mqm\": $total_mqm, \"rdm\": $total_rdm, \"elite\": \"$elite\"}";
} else {
	print "<p><b>Remember, accuracy is not guaranteed.</b> But it should be pretty close.  Oh, and this site is not affiliated with Delta Air Lines.</p>\n";
	print "</div>";
	emit_google_analytics_snippet();
	print "</body></html>";
}
?>
