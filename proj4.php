<?php
/** Some constants that PROJ uses for the WGS84 ellipse projection.*/
define('G_A', 6378137);
define('G_ONEF', 0.99664718933525254);
define('G_FLAT4', 0.00083820266618686579);
define('G_FLAT64',1.756459274006926e-07);
define('METERS_TO_MILES',0.00062136994949494966);
define('DTOL', 1e-12);
define('SPI', 3.14159265359);

/**
 * This method was "adapted" (read: blatantly ripped off) from geod_inv() in PROJ.4.
 * Happily its license allows this.  I have no idea how this algorithm works.
 */
function distance_in_miles ($lat1, $lon1, $lat2, $lon2)
{
	// Convert to radians
	$lat1 = $lat1 * (SPI / 180);
	$lat2 = $lat2 * (SPI / 180);
	$lon1 = $lon1 * (SPI / 180);
	$lon2 = $lon2 * (SPI / 180);
	//double $th1,$th2,thm,dthm,dlamm,dlam,$sindlamm,$costhm,$sinthm,$cosdthm,
	//	$sindthm,L,E,$cosd,d,X,Y,T,$sind,D,A,B;
	// double tandlammp,u,v;
	//$th1 = 0; $th2 = 0; $thm = 0; $dthm = 0; $dlamm = 0; $dlam = 0;
	//$sindlamm = 0; $costhm = 0; $sinthm = 0; $cosdthm = 0;
	//	$sindthm,L,E,$cosd,d,X,Y,T,$sind,D,A,B;
	// double tandlammp,u,v;

	// double al12, al21;
	// double $geod_S;
	$ellipse = TRUE;

	if ($ellipse) {
		$th1 = atan(G_ONEF * tan($lat1));
		$th2 = atan(G_ONEF * tan($lat2));
	} else {
		$th1 = $lat1;
		$th2 = $lat2;
	}
	$thm = .5 * ($th1 + $th2);
	$dthm = .5 * ($th2 - $th1);
	$dlamm = .5 * ( $dlam = adjlon($lon2 - $lon1) );
	if (abs($dlam) < DTOL && abs($dthm) < DTOL)
	{
		// al12 =  al21 = 0.;
		$geod_S = 0.;
		// return Double.NaN;
		return -42;
	}
	
	$sindlamm = sin($dlamm);
	$costhm = cos($thm);
	$sinthm = sin($thm);
	$cosdthm = cos($dthm);
	$sindthm = sin($dthm);
	$L = $sindthm * $sindthm + ($cosdthm * $cosdthm - $sinthm * $sinthm) * $sindlamm * $sindlamm;
	$d = acos($cosd = 1 - $L - $L);
	if ($ellipse)
	{
		// Holy crap!
		$E = $cosd + $cosd;
		$sind = sin( $d );
		$Y = $sinthm * $cosdthm;
		$Y *= ($Y + $Y) / (1. - $L);
		$T = $sindthm * $costhm;
		$T *= ($T + $T) / $L;
		$X = $Y + $T;
		$Y -= $T;
		$T = $d / $sind;
		$D = 4. * $T * $T;
		$A = $D * $E;
		$B = $D + $D;
		$geod_S = G_A * $sind * ($T - G_FLAT4 * ($T * $X - $Y) +
				G_FLAT64 * ($X * ($A + ($T - .5 * ($A - $E)) * $X) -
					$Y * ($B + $E * $Y) + $D * $X * $Y));
		//tandlammp = Math.tan(.5 * (dlam - .25 * (Y + Y - E * (4. - X)) *
		//	(G_FLAT2 * T + G_FLAT64 * (32. * T - (20. * T - A)
		//	* X - (B + 4.) * Y)) * Math.tan(dlam)));
	} else {
		$geod_S = G_FLAT64 * $d;
		//tandlammp = Math.tan(dlamm);
	}
	// u = Math.atan2($sindthm , (tandlammp * $costhm));
	// v = Math.atan2($cosdthm , (tandlammp * $sinthm));
	// al12 = adjlon(2 * SPI + v - u);
	// al21 = adjlon(2 * SPI - v - u);

	return $geod_S * METERS_TO_MILES;
}

/**
 * This does...something.  Apparently, it's important.
 * (Taken from PROJ.4)
 */
function adjlon ($lon)
{
	if(abs($lon) <= SPI)
		return $lon;
	$lon += SPI;  /* adjust to 0..2pi rad */
	$lon -= 2 * SPI * floor($lon / (2 * SPI)); /* remove integral # of 'revolutions'*/
	$lon -= SPI;  /* adjust back to -pi..pi rad */
	return $lon;
}
?>
