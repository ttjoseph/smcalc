<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="site.css" type="text/css" media="screen" title="" charset="utf-8">
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
	<title>Unofficial SkyMiles calculator</title>
	<script type="text/javascript">
var REQUEST_DELAY = 500;
var TimerID = null;

function showQuestionMarks() {
	$("#ajax_miles_flown").html("?");
	$("#ajax_mqm").html("?");
	$("#ajax_rdm").html("?");
	$("#ajax_elite").html("?").removeClass();

}

function doAjax() {
	if($("#route").val() === "") return;
	$.getJSON("dist.php", $("#trips").serialize() + "&ajax", function(result) {
		if(!result) {
			showQuestionMarks();
			$("#ajax_results").css("opacity", "1.0");
			return;
		}
		$("#ajax_miles_flown").html(result.miles_flown);
		$("#ajax_mqm").html(result.mqm);
		$("#ajax_rdm").html(result.rdm);
		$("#ajax_elite").html(result.elite).removeClass().addClass("elite-" + result.elite);
		$("#ajax_results").css("opacity", "1.0");
	});
}

function updateAjaxResult() {
	if(TimerID != null) {
		clearTimeout(TimerID);
	}
	$("#ajax_results").css("opacity", "0.2");
	TimerID = setTimeout("doAjax()", REQUEST_DELAY);
}

$(document).ready(function() {
	doAjax();
	$("textarea,input").keyup(updateAjaxResult);
	$("select").change(updateAjaxResult);
});
	</script>
</head>
<body id="main" style="background: #e0e0e0 url(shattered.png); margin-top: 0px;">
<div id="outer">
	<h1>Unofficial SkyMiles calculator</h1>
	
	<form id="trips" action="dist.php" method="get" accept-charset="utf-8">
	<table><tr><td>
		Routes:<br/>
		<textarea id="route" name="route" rows="8" cols="60" placeholder="Example: jfk-atl-sfo-atl-jfk"></textarea><br/>

		Begin with
		<input type="text" name="start_mqm" value="0" id="start_mqm" size="6"> MQMs and
		<input type="text" name="start_rdm" value="0" id="start_rdm" size="6"> redeemable miles.<br/>
		Default fare: <input type="text" name="default_fare" value="T" id="default_fare" size="1"> on
		<input type="text" name="default_carrier" id="default_carrier" value="DL" size="2">.
		Elite status:
		<select name="elite">
			<option value="peon">None</option>
			<option value="silver">Silver Medallion</option>
			<option value="gold">Gold Medallion</option>
			<option value="platinum">Platinum Medallion</option>
			<option value="diamond">Diamond Medallion</option>
		</select>
		
	</td><td>
<div id="ajax_results">
Miles flown: <span id="ajax_miles_flown">?</span><br/>
MQMs: <span id="ajax_mqm" class="miles-eqm">?</span><br/>
RDMs: <span id="ajax_rdm" class="miles-rdm">?</span><br/>
Elite status: <span id="ajax_elite">?</span>
</div>
<div style="padding-left: 20px"><input type="submit" value="Show details"></div>
</td></tr></table>

	</form>

</div>

<div style="padding: 1em; -webkit-columns: 3; -moz-column-count: 3; padding-top: 2em;">

<p><b>Hello.</b> This tool calculates how many SkyMiles are earned flying on Delta and its partners.  Plan your trips &mdash; or mileage runs &mdash; to maximize mileage earning, or just see where you'll end up this year with your miles obsession.</p>

<p><b>Type your route.</b> <code>jfk-atl-sfo-atl-jfk</code> is a round trip from New York JFK to San Francisco with a stop in Atlanta, all on Delta, using the "Default fare" specified above. Enter more trips on separate lines.</p>

<p>Specify carrier and fare bucket by typing something like <code>ke.f</code> between the airport codes; that means that the segment is on Korean Air in the F fare bucket. Omit the carrier code to default to DL, such that <code>y</code> means Y on Delta. So, <code>jfk-k-atl-ke.c-icn</code> means New York JFK to Atlanta on a K fare on Delta, then onward to Seoul Incheon on Korean Air on a C fare.</p>

<p>To see your <b>cost per mile</b> for a trip, type the fare too. For example: <code>lga-mdw-lga $199</code>.
<b>Revenue-based redeemable miles</b> will be estimated for all-Delta trips. If you specify more than one dollar amount on the line, only the first will be used for this calculation, since Delta does not give you miles for government taxes and fees. The sum will be used for the other costs per mile.</p>

<p>This tool does not currently take into account <a href="https://www.delta.com/content/www/en_US/skymiles/earn-miles/earn-miles-with-exception-fares.html">Delta exception fares</a> such as consolidator and cruise fares which can still earn redeemable miles based on distance flown.</p>

<p>If you received <b>bonus miles</b> include that amount as well.  For example: <code>jfk-lax-jfk 500</code> would specify a trip from New York to Los Angeles that you got a 500-mile redeemable miles bonus for. 
</p>

<p> Delta's guide to miles earning on its own tickets is
<a href="https://www.delta.com/content/www/en_US/skymiles/earn-miles/earn-miles-with-delta.html">here</a>
and with partners is
<a href="https://www.delta.com/content/www/en_US/skymiles/earn-miles/earn-miles-with-partners/airlines.html">here</a>. I make my best guess, but remember that <b>this website is not affiliated in any way with <a href="http://www.delta.com">Delta Air Lines</a>.</b> No guarantee of accuracy is made; Delta is the final authority on how many miles they actually award.  A discrepancy of a few miles can be expected because I probably round numbers a bit differently than Delta does.  If you get a really incorrect result, I'd appreciate hearing about it so I can fix it (I'm <b>ttjoseph</b> on FlyerTalk; send me a PM).</p>

<p><b>Also,</b> if you're a hardcore mileage runner, try out my <a href="../routemax">route maximizer</a>. If you use a Mac, I wrote a <a href="/airportcodes/">Dashboard widget</a> that you can use to look up IATA airport and airline codes.</p>

</div>

<?php include_once("google-analytics.php") ?>
</body>

</html>
